<?php

class GmapsModel extends CI_Model{

    private $locais = array();
    private $configs = array();

    function __construct(){
        parent::__construct();
    }

    /**
     * @return array
     */
    public function getLocais()
    {
        $data['locais'] = $this->locais;
        $data['configs'] = $this->configs;
        return $data;
    }

    /**
     * Adiciona um único local
     * @param float $lat = latitude do local
     * @param float $lgn = longitude do local
     */
    public function setLocal($lat, $lgn)
    {
        $this->locais[] = 'var local'.sizeof($this->locais).' = {lat: '.$lat.', lng: '.$lgn.'};';
        $this->configs[] = 'var marker = new google.maps.Marker({
                                            position: local'.(sizeof($this->locais)-1).',
                                            map: map
                                        });';
    }

    /**
     * Adiciona vários locais de uma vez só, porem a estrutura do array precisa ser:
     * $data['lat'][] = Armazena a latitude
     * $data['lgn'][] = Armazena a longitude
     * @return array $data
     */
    public function createLocais($data){
        for($i=0; $i<sizeof($data['lat']); $i++){
            $this->locais[] = 'var local'.sizeof($this->locais).' = {lat: '.$data['lat'][$i].', lng: '.$data['lgn'][$i].'};';
            $this->configs[] = 'var marker = new google.maps.Marker({
                                            position: local'.(sizeof($this->locais)-1).',
                                            map: map
                                        });';
        }

        return $this->getLocais();
    }

    /**
     * Exemplo de array que deve ser recebido para utilizar a função createLocais($data)
     * @return array
     */
    public function returnDataExample(){
        $data['lat'][] = -22.363;
        $data['lat'][] = -21.363;
        $data['lat'][] = -20.363;
        $data['lat'][] = -19.363;
        $data['lgn'][] = 132.044;
        $data['lgn'][] = 131.044;
        $data['lgn'][] = 130.044;
        $data['lgn'][] = 129.044;
        return $data;
    }

}