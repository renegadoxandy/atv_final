<?php

class Gmaps extends MY_Controller{

    public function doc(){

        $this->load->model('GmapsModel', 'model');

        $locais = $this->model->returnDataExample();
        $datalocais = $this->model->createLocais($locais);
        $data['gmaps'] = $this->load->view('index.php', $datalocais, true);

        $html = $this->load->view('documentation.php', $data, true);
        $this->show($html, false, false);
    }

    public function example(){
        $this->load->model('GmapsModel', 'model');

        $this->model->setLocal(-22.363,132.044);
        $this->model->setLocal(-21.363,131.044);

        $data = $this->model->getLocais();

        $html = $this->load->view('index.php', $data, true);
        echo $html;
    }

    public function example2(){
        $this->load->model('GmapsModel', 'model');

        $locais = $this->model->returnDataExample();

        $data = $this->model->createLocais($locais);

        $html = $this->load->view('index.php', $data, true);
        echo $html;
    }
}