
<div class="container mt-3">

    <section id="gmaps" class="mb-5">
        <h4 class="title"><strong>Google Maps com marcadores</strong></h4>
        <?= $gmaps ?>

    </section>

<section id="controller">

    <h4 class="title"><strong>1. Controller</strong></h4>
    <p>Vamos supor que você deseja usar este componente em um controlador e no método 'gmap'.</p>

    <section>
        <p>1.1 Defina o método</p>
        <div class="card pl-2 pt-1 pb-1 mb-2"><pre><code>
public function gmap(){

}
            </code></pre>
        </div>
        <p>1.2 Carregue o model GmapsModel</p>
        <div class="card pl-2 pt-1 pb-1 mb-2"><pre><code>
public function gmap(){
    $this->load->model('GmapsModel', 'model');
}
            </code></pre>
        </div>
        <p>1.3 Prepare um array com as latitudes e longitudes dos locais que você deseja marcar, exemplo de array (chamaremos esse array de <strong>$locais</strong>):</p>
        <div class="card pl-2 pt-1 pb-1 mb-2"><pre><code>
Array
(
    [lat] => Array
        (
            [0] => -22.363
            [1] => -21.363
            [2] => -20.363
            [3] => -19.363
        )

    [lgn] => Array
        (
            [0] => 132.044
            [1] => 131.044
            [2] => 130.044
            [3] => 129.044
        )

)
            </code></pre>
        </div>
        <p>1.4 Utilize a função createLocais($agr), e passe como argumento o array $locais</p>
        <div class="card pl-2 pt-1 pb-1 mb-2"><pre><code>
public function gmap(){
    $this->load->model('GmapsModel', 'model');
    //$locais = (RECEBA O ARRAY COM AS LATITUDES E LONGITUDES);
    $data = $this->model->createLocais($locais);
}
            </code></pre>
        </div>
        <p>1.5 Passe $data na view gmaps/views/index.php</p>
        <div class="card pl-2 pt-1 pb-1 mb-2"><pre><code>
public function gmap(){
    $this->load->model('GmapsModel', 'model');
    //$locais = (RECEBA O ARRAY COM AS LATITUDES E LONGITUDES);
    $data = $this->model->createLocais($locais);
    $html = $this->load->view('index.php', $data, true);
}
            </code></pre>
        </div>

        <p>1.6 Exiba a view</p>
        <div class="card pl-2 pt-1 pb-1 mb-2"><pre><code>
public function gmap(){
    $this->load->model('GmapsModel', 'model');
    //$locais = (RECEBA O ARRAY COM AS LATITUDES E LONGITUDES);
    $data = $this->model->createLocais($locais);
    $html = $this->load->view('index.php', $data, true);
    echo $html;
}
            </code></pre>
        </div>
    </section>

</section>

</div>